package com.yogisyaputra.java.spring.simple.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author yogilobay <yogisyaputra@com>
 * @Since 11/21/19
 * @Time 8:07 PM
 * @Encoding UTF-8
 * @Project yogisyaputra-java-spring
 * @Package
 */

@Controller
public class SimpleSchedulerController {
    @Scheduled(cron = "*/10 * * * * *")
    public void cronJobSch() throws URISyntaxException, IOException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        System.out.println("Java cron job expression:::: " + strDate);
    }

    @Scheduled(fixedRate = 1000)
    public void fixedRateSch() throws URISyntaxException, IOException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        System.out.println("Fixed Rate scheduler:: " + strDate);

    }

    @Scheduled(fixedDelay = 1000, initialDelay = 1000)
    public void fixedDelaySch() throws URISyntaxException, IOException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        System.out.println("Fixed Delay scheduler:: " + strDate);

    }
}
