# Spring Boot - Scheduling

## Java Cron Expression
Java Cron expressions are used to configure the instances of CronTrigger, a subclass of org.quartz.Trigger. 
The @Scheduled annotation is used to trigger the scheduler for a specific time period.

## Fixed Rate
Fixed Rate scheduler is used to execute the tasks at the specific time. It does not wait for the completion of previous task. The values should be in milliseconds. The sample code is shown here −

## Fixed Delay
Fixed Delay scheduler is used to execute the tasks at a specific time. It should wait for the previous task completion. The values should be in milliseconds. A sample code is shown here −

